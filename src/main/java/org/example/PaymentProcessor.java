package org.example;

public class PaymentProcessor {


//    we should not use concrete reference
//    we should only use abstract reference
//    private PaypalPaymentGateway gateway;

    private Gateway gateway;

    public PaymentProcessor(Gateway gateway){

        this.gateway = gateway;
    }
    public void makePayment(int amount) {

        gateway.makePayment(amount);
    }

    public String getPaymentStatus(int transactionID) {

        return gateway.getPaymentStatus(transactionID);
    }

}

//never instanciate a dependency inside a dependent class
//inject the dependency

package org.example;

public interface Gateway {
    public void makePayment(double amount);
    public String getPaymentStatus(int transactionID);
}
